import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';
import ReactTestUtils from 'react-addons-test-utils';

import {WeatherData} from '../src/app/components/WeatherData.jsx';
import {Home} from '../src/app/components/Home.jsx';


describe('<Home/>', function () {
    
  beforeEach(function() {
    this.state = {};
  });
    
  it('contains an <WeatherData/> component', function () {
    const wrapper = mount(<Home />);
    expect(wrapper.find(WeatherData)).to.have.length(1);
  });
  it('should have an initial state as a object', function () {
    const wrapper = mount(<Home />);
    expect(wrapper.state()).to.be.an('object');
  });
   
});