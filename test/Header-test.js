import React from 'react';
import { mount, shallow } from 'enzyme';
import {expect} from 'chai';
import {Header} from '../src/app/components/Header.jsx';


describe('<Header/>', function () {
  it('should have a classname => pageHeader', function () {
    const wrapper = shallow(<Header />);
    expect(wrapper.find('div').hasClass('page-header')).to.equal(true);
  });
it('Should have the header text',function(){
   const wrapper = shallow(<div><h1>Five Days Weather App</h1></div>);
   expect(wrapper.text()).to.equal('Five Days Weather App') 
    });
});