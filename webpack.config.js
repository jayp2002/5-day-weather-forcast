var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'dist');
var APP_DIR = path.resolve(__dirname, 'src');

var config = {
  entry: APP_DIR + '/app/index.jsx',
  output: {
    path: BUILD_DIR + "/app",
    filename: 'bundle.js',
    publicPath: '/app/'
  },
  devServer: {
    inline:true,
    port: 8000
  },
  plugins: [
    new webpack.ProvidePlugin({
      jQuery: 'jquery'
    })
  ],
  module : {
    loaders : [
      {
        test : /\.jsx?/,
        include : APP_DIR,
        exclude: /(node_modules|bower_components)/,
        loader : 'babel',
        query: {
          presets: ['react', 'es2015']
        }
      }
    ]
  }
};

module.exports = config;