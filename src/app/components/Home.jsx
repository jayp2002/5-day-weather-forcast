import React from "react";
import Request from "superagent";

import {WeatherData} from './WeatherData.jsx';


export class Home extends React.Component {

  constructor(){
    super();
    this.state = {};
  }

  componentDidMount(){
    let url = 'http://api.openweathermap.org/data/2.5/forecast?id=2643743&appid=416f21735638892910fc788dbd92dc24';
    Request.get(url).then((response) =>{
      this.setState({
        city: response.body.city.name,
        lists: response.body.list
      });
    });
  }

  render() {
    return (
        <div className="jumbotron">
          <h3>Weather For: {this.state.city}</h3>
          <WeatherData lists={this.state.lists} />
        </div>
    );
  }
}