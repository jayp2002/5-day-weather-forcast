/**
 * Created by jayp2002 on 24/01/2017.
 */
import React from "react";


export class WeatherData extends React.Component {

  render() {
    if (!this.props.lists) {
      return null;
    }
    return (
      <div>
        <ul className="list-group">
          {
            this.props.lists.map(function(list) {
              return (
                  <li className="list-group-item">Time and date: {list.dt_txt}
                    <ul>
                      <img src={"http://openweathermap.org/img/w/" + list.weather[0].icon + ".png"} />
                      <li>Description: {list.weather[0].description}</li>
                      <li>Temprature (Kelvin): {list.main.temp}</li>
                      <li>Humidity (%): {list.main.humidity} </li>
                    </ul>
                  </li>
              )
            })
          }
        </ul>
      </div>
    )
  }
}