# 5 day weather forecast
Follow the following steps to run the application:

### Install packages ###
npm install

### Start Server ###
npm start

### To see the applicatio ###
go to http://localhost:8000/

### To run unit test ###
npm test

### Future enhancement: ###
1. add a input for user to enter country/country code to get specific weather forecast, with submit button to make a call to API and get the right data
1. improve on the data view by adding some sort of graph, add icons for temprature give more attention on UI
1. have some error handling in place when no data is returned from API
1. create seperate views for data eg, hourly/daily
1. convert date/data into more readable format eg, convert temp into degrees,
1. group data by day
1. create a specific API file, rather than including it in the Home.jsx to keep the code structure clean